//Libreria express
var express = require('express');
var app = express();
//Libreria request-json
var requestJson = require('request-json');
//Deaclara la URL base para todo el programa
var urlBase = '/colbbva/v1/';
//Importa la libreria body-parser
var bodyParser = require('body-parser');
app.use(bodyParser.json());
//Variables para definir la URL de MLab
var baseMlab = 'https://api.mlab.com/api/1/databases/colbbva_oipp/collections/';
var apiKey = 'apiKey=80aJU7mCzZprpI8uqblWDwEl86jCEaNs';
//Puerto por el que esta escuchando la aplicacion
var port = process.env.PORT || 4000;
app.listen(port);
console.log('listen port ' + port);

//login desde polymer a mLab ***************************************************

//POST login usando API mLab para conexion con MongoDB
app.post(urlBase + 'login', function(req, res) {
    var email = req.body.email //Variable con el email
    var password = req.body.password //Var con el password
    var query = 'q={"email":"' + email + '","password":"' + password + '"}' //queryString: se utiliza para formar la URL de consulta para mLab
    clientMlab = requestJson.createClient(baseMlab + "/user?" + query + "&l=1&" + apiKey); //Peticion a mLab
    //console.log(baseMlab + "/user?" + query + "&l=1&" + apiKey);
    clientMlab.get('', function(err, resM, body) { //Consultamos si existe usuario
      if (!err) {
        if (body.length == 1){ // Si encuentra el usuario
          clientMlab = requestJson.createClient(baseMlab + "/user");
          var logged = '{"$set":{"logged":true}}'; //Con el set agregamos el estado logged
          clientMlab.put('?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(logged), function(errP, resP, bodyP) { //PUT agregar logged
              //console.log(body);
              res.send(body); //Response
          });
        }
        else {
          res.status(404).send('User not found'); //No hay resultados
        }
      }
    });
});

//POST Login v1 alternativo con mLab
app.post(urlBase + 'login8', function(req, res) {
 const httpClient = requestJson.createClient(baseMlab);
 var queryString = 'q={"email":' + '"' + req.body.email + '"' + '}&';
 httpClient.get('user?' + queryString + apiKey, function(e, reqMlab, body){
   if(!body.length || !Array.isArray(body)){
     res.json({"msg":"Error email"});
   }else{
     if(body[0].password !== req.body.password){
       res.json({"msg":"Error password"});
     }else{
       var login = '{"$set":{"logged":true}}';
       httpClient.put('user?q={"id":' + '"' + body[0].id + '"' + '}&' + apiKey, JSON.parse(login), function(e, reqMlab, body) {
             res.status(200).send(body);
         });
     }
   }
 });
});

//logout desde polymer a mLab ***************************************************

//POST logout usando API mLab para conexion con MongoDB
app.post(urlBase + 'logout', function(req, res) {
    var id = req.body.id; //recibimos el id del usuario
    var query = 'q={"id":' + id + ', "logged":true}';
    httpClient = requestJson.createClient(baseMlab + "/user?" + query + "&l=1&" + apiKey);
    httpClient.get('', function(err, resM, body) {
      if (!err) {
        if (body.length == 1){ //encuentra el usuario con el atributo logged = true
          httpClient = requestJson.createClient(baseMlab + "/user")
          var logged = '{"$set":{"logged":false}}';
          httpClient.put('?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(logged), function(errP, resP, bodyP) {
              //res.send({"logout":"ok", "id":body[0].id});
              res.send(body); //Response
          });
        }
        else {
          res.status(200).send('User not logged');
        }
      }
    });
});

//collection user **************************************************************

//GET users a traves de mLab
app.get(urlBase + 'users',
  function(request, response){
      var httpClient = requestJson.createClient(baseMlab);
      var queryString = 'f={"_id":0}&';
      httpClient.get('user?'+ queryString + apiKey,
          function(e, respuestaMLab, body){
              var responseUser = body;
              responseUser = !e ? body : {"msg":"Error"};
              response.send(responseUser);
          });
});

//POST users con mLab
app.post(urlBase + 'users', function(req, res) {
  var newUser = {
    "id" : req.body.id,
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
    "password" : req.body.password
  };
  var clientMlab = requestJson.createClient(baseMlab + "/user?" + apiKey);
  clientMlab.post('', req.body, function(err, resM, body) {
    res.send(body);
  });
});

//GET users a traves de mLab con parametros
app.get(urlBase + 'users/:id',
  function(request, response){
      var idParam = request.params.id;
      var httpClient = requestJson.createClient(baseMlab);
      var queryString = 'q={"id":' + idParam + '}&';
      httpClient.get('user?'+ queryString + apiKey,
          function(e, respuestaMLab, body){
              var answer = body[0];
              response.send(answer);
          });
});

//GET users a traves de mLab con parametros
app.get(urlBase + 'users/:id/accounts',
  function(request, response){
      var idParam = request.params.id;
      var httpClient = requestJson.createClient(baseMlab);
      var queryString = 'q={"user_id":' + idParam + '}&';
      httpClient.get('account?'+ queryString + apiKey,
          function(e, respuestaMLab, body){
              var answer = body;
              response.send(answer);
          });
});

//collection account ***********************************************************

//GET account a traves de mLab
app.get(urlBase + 'accounts',
  function(request, response){
      //console.log('GET /Accounts');
      var httpClient = requestJson.createClient(baseMlab);
      var queryString = 'f={"_id":0}&';
      httpClient.get('account?'+ queryString + apiKey,
          function(e, respuestaMLab, body){
              //console.log('Error ' + e);
              //console.log('Body ' + body);
              //console.log('Respuesta MLab ' + respuestaMLab);
              var respuesta = body;
              respuesta = !e ? body : {"msg":"Error"};
              response.send(respuesta);
          });
});

//GET account a traves de mLab con parametros
app.get(urlBase + 'accounts/:id',
  function(request, response){
      var idParam = request.params.id;
      var httpClient = requestJson.createClient(baseMlab);
      var queryString = 'q={"user_id":' + idParam + '}&';
      httpClient.get('account?'+ queryString + apiKey,
          function(e, respuestaMLab, body){
              var answer = body[0];
              response.send(answer);
          });
});

//collection movement **********************************************************

//GET movement a traves de mLab
app.get(urlBase + 'movements',
  function(request, response){
      //console.log('GET /Movements');
      var httpClient = requestJson.createClient(baseMlab);
      var queryString = 'f={"_id":0}&';
      httpClient.get('movement?'+ queryString + apiKey,
          function(e, respuestaMLab, body){
              //console.log('Error ' + e);
              //console.log('Body ' + body);
              //console.log('Respuesta MLab ' + respuestaMLab);
              var respuesta = body;
              respuesta = !e ? body : {"msg":"Error al hacer la consulta"};
              response.send(respuesta);
          });
});

//GET movement a traves de mLab con parametros
app.get(urlBase + 'movements/:id',
  function(request, response){
      var idParam = request.params.id;
      var httpClient = requestJson.createClient(baseMlab);
      var queryString = 'q={"id_mov":' + idParam + '}&';
      httpClient.get('movement?'+ queryString + apiKey,
          function(e, respuestaMLab, body){
              var answer = body[0];
              response.send(answer);
          });
});

//GET con parámetro IBAN movements
app.get(urlBase + 'movements/:iban',
function (req, res) {
  console.log(req.params.iban);
  var id_mov = req.params.iban;
  var queryString = 'q={"IBAN":' + id_mov + '}&'
  var httpClient = requestJson.createClient(baseMlab);
  httpClient.get('movement?' + queryString + apiKey,
  function(err, respuestaMlab, body) {
    //console.log("Error: " + err);
    //console.log("Respuesta Mlab: " + respuestaMlab);
    //console.log("Body: " + body);
    var answer = body[0];
    res.send(answer);
    });
});
