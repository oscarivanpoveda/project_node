#Imagen inicial a partir de la cual creamos nuestra imagen
FROM node
#Se define el directorio del contenedor
WORKDIR /project_tech
#Adiciona el contenido del proyecto en el directorio del contenedor
ADD . /project_tech
#Puerto por el que escucha el contenedor
EXPOSE 4000
#Comando para lanzar nuestra API REST
CMD ["node", "server.js"]
