//import de las librerias de test
var mocha = require('mocha'); //libreria mocha
var chai = require('chai'); //libreria chai
var chaiHttp = require('chai-http'); //libreia chai-http
var server = require('../server'); //nombre del archivo que ejecutamos
//Instruccion que me define si es lo que espero al final de la prueba
var should = chai.should();
//Configurar chai con modulo HTTP
chai.use(chaiHttp);
//Conjunto de pruebas
describe('Test Colombia to BBVA', () => {
  //con el it genera los casos de prueba
  it('BBVA OK', (done) => {
    chai.request('http://www.bbva.com')
    .get('/')
    .end((err, res) => {
      res.should.have.status(200);
      done();
    })
  });
});
//Pruebas para users del API
describe('Tests API NodeJs', () => {
  it('Users Array ', (done) => {
    chai.request('http://localhost:4000')
    .get('/colbbva/v1/users')
    .end((err, res) => {
      res.body.should.be.a('array');
      done();
    })
  });

  it('One Element', (done) => {
    chai.request('http://localhost:4000')
    .get('/colbbva/v1/users')
    .end((err, res) => {
      res.body.length.should.be.gte(1);
      done();
    })
  });

  it('Test on One Element', (done) => {
    chai.request('http://localhost:4000')
    .get('/colbbva/v1/users')
    .end((err, res) => {
      //console.log(res.body[0]);
      res.body[0].should.have.property('first_name');
      res.body[0].should.have.property('last_name');
      done();
    })
  });

  it('Test POST Users', (done) => {
    chai.request('http://localhost:4000')
    .post('/colbbva/v1/users')
    .send('{"id": 22, "first_name": "Carolina", "last_name": "Moreno", "email": "carolina.moreno@bbva.com", "password": "maria020"}')
    .end((err, res) => {
      //console.log(res.body);
      done();
    })
  });

});
//Test para login
describe('Test Colombia to BBVA', () => {
  it('My API', (done) => {
    chai.request('http://localhost:4000')
    .post('/colbbva/v1/login')
    .end((err, res) => {
      res.should.have.status(404);
      done();
    })
  });
});
